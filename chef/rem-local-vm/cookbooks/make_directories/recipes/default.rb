#
# Cookbook Name:: make_directories
# Recipe:: default
#
# Copyright 2014, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#				


dirs = node['make_directories']['directories_to_create']

if dirs.nil? | dirs.empty? then
	raise "node['make_directories']['directories_to_create] is nil or empty!"
end

user_name = node['make_directories']['user']

dirs.each do |path|
	directory path do 
		owner user_name
		mode 00755
		action :create
		recursive true
	end
end
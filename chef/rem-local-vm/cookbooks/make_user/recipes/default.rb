#
# Cookbook Name:: make_user
# Recipe:: default
#
# Copyright 2014, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

user_name = node['make_user']['user']
pw = node['make_user']['password']

user user_name do
  home "/home/" + user_name
  shell "/bin/bash"
  password pw
  action :create
end
Introduction
------------

This project automates the Reimbursement Manager's Virtual Machine.

**Note** - you'll need to follow the instructions [here](https://mhsprod.jira.com/wiki/display/RMPD/Installation+of+ReM%27s+Dev+VM+Environment "Dev VM Environment") before spinning up and provisioning the REM VM.

Commands
--------

To view the available VM instances, run:

```kitchen list```

In order to spin up a VM, execute:

```kitchen create NAME_OF_VM_INSTANCE```

In order to provision, i.e. run the Chef cookbooks, a VM, run:

```kitchen converge NAME_OF_VM_INSTANCE```

To destroy, i.e. wipe out the entire VM, run:

```kitchen destroy NAME_OF_VM_INSTANCE```

**Example**
-------

Running ```kithen list``` shows me that there's a single VM instance.

```
$kitchen list
Instance                 Driver   Provisioner  Last Action
rem-local-vm-centos-64  Vagrant  ChefSolo     <Not Created>
```

To spin up and provision (install the Chef cookbooks' software), run:

```kitchen converge rem-local-vm-centos-64```

How to Power On & Turn Off VM(s)
--------------------------------

To simply turn on a VM **without** provisioning it, simply run:

```kitchen create NAME_OF_VM_INSTANCE```

TO turn off the VM, open VirtualBox:
1. Right-click VM 
2. Left-click "Close"

Changing Maven Run-time Version
-------------------------------

After provisioning the VM, there will be 2 versions of Maven installed.

```
$ alternatives --display mvn
mvn - status is auto.
 link currently points to /usr/local/maven_dev-3.2.1
/usr/local/maven_dev-3.2.1 - priority 100
/usr/local/maven_qa-3.0.5 - priority 50
Current `best' version is /usr/local/maven_dev-3.2.1.
```

By default, maven-3.2.1 will be active. To change to 3.0.5 (QA), run the following
command:
```
$ sudo alternatives --set mvn /usr/local/maven_qa-3.0.5

$ mvn -version
Apache Maven 3.0.5 (r01de14724cdef164cd33c7c8c2fe155faf9602da; 2013-02-19 13:51:28+0000)
Maven home: /u01/nm/rm/3rd/maven
Java version: 1.7.0_55, vendor: Oracle Corporation
Java home: /u01/nm/rm/3rd/jdk1.7.0_55/jre
Default locale: en_US, platform encoding: UTF-8
OS name: "linux", version: "2.6.32-358.el6.x86_64", arch: "amd64", family: "unix"
```
To switch back:
```
$ sudo alternatives --set mvn /usr/local/maven_dev-3.2.1

$ mvn -version
Apache Maven 3.2.1 (ea8b2b07643dbb1b84b6d16e1f08391b666bc1e9; 2014-02-14T17:37:52+00:00)
Maven home: /u01/nm/rm/3rd/maven
Java version: 1.7.0_55, vendor: Oracle Corporation
Java home: /u01/nm/rm/3rd/jdk1.7.0_55/jre
Default locale: en_US, platform encoding: UTF-8
OS name: "linux", version: "2.6.32-358.el6.x86_64", arch: "amd64", family: "unix"
```
#
# Cookbook Name:: tomcat
# Recipe:: default
#
# Copyright 2014, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

include_recipe 'ark::default'

tomcat_path = node['tomcat']['path']

ark 'tomcat' do
  url node['tomcat']['url']
  version node['tomcat']['version']
  path tomcat_path
  owner node['tomcat']['owner'] 
  action :put
end

tomcat_bin = tomcat_path + '/tomcat/bin'

magic_shell_environment "PATH" do
	value tomcat_bin + ":$PATH"
end

tomcat_users_xml_vm_loc = tomcat_path + '/tomcat/conf/tomcat-users.xml'

# TODO: replace with Chef template
cookbook_file tomcat_users_xml_vm_loc do
	source 'tomcat-users.xml'
	owner node['tomcat']['owner'] 
	group node['tomcat']['owner'] 
	mode 0755
end
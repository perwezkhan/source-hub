# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
        . /etc/bashrc
fi

# User specific aliases and functions
# note - implementing in `cookbooks/rem/recipes/default.rb`

#. .aliases.sh

git config --global url."https://".insteadOf git://

#
# Cookbook Name:: rem
# Recipe:: default
#
# Copyright 2014, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

include_recipe "nodejs::npm"
packages = node['rem']['npm_packages']

if packages.nil? | packages.empty? then
	raise "node['rem']['npm_packages'] is nil or empty!"
end

install_path = node['rem']['path']

packages.each do |package|
	nodejs_npm  package do
		action :install
	end
end

# Set java & javac run-time since, occasionally, Red Hat's JDK is active on first provision
# Note - the socrata/java cookbook installs java into alternatives. If this cookbook changes,
# then the below code will need to change to incorporate installing java into alternatives
if platform_family?("rhel")
	java_home = node['rem']['java']

	execute "set java run-time" do
		command "alternatives --set java " + java_home
		not_if (java_home.empty? )
	end

	javac_home = node['rem']['javac']

	execute "set java run-time" do
		command "alternatives --set javac " + javac_home
		not_if ( javac_home.empty? )
	end
end

user = node['rem']['user']
dir  = node['rem']['change_permissions_dir']

execute "recursive chown shared directory to rem.user" do
	command "chown -R " + user + " " + dir
	not_if( dir.empty? )
end

group user do
	action :create
end

execute "recursive chgrp shared directory to rem.user" do
	command "chgrp " + user + " -R " + dir
	not_if( dir.empty? )
end

# install libraries necessary for REM front-end build
# move to attributes: [rem][yum_packages]
if platform_family?("rhel")
	yum_packages = node['rem']['yum_packages']

	yum_packages.each do |p|
		package p do
			action :install	
		end
	end

	# Note - pngquant was not found in yum repo
end

user = node['rem']['user']
files = node['rem']['files_copy_to_guest']

if files.nil? | files.empty? then
	Chef::Log.warn("node['rem']['files_copy_to_guest'] is nil or empty!")
end

files.each do |vm_dest, host_src|
	log vm_dest + " , " + host_src
	cookbook_file vm_dest do 
		source host_src
		owner user
		group user
		mode 0755	
	end		
end 

magic_shell_environment "RM_REPOS" do
	value node['rem']['rm_repos']
end

magic_shell_environment "RM_HOME" do
	value node['rem']['rm_home']
end
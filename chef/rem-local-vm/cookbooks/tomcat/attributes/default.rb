default['tomcat']['url']                   = 'https://anthill.fmgtech.com/installers/rem/apache-tomcat-7.0.53.tar.gz'
default['tomcat']['owner']                 = 'rem'
default['tomcat']['version']               = '7.0.53'
default['tomcat']['path']                  = '/u01/nm/rm/3rd/tomcat7.0.53/'

Please don't modify base/app.js or base/app.html. When running a separate test, copy the 'base' folder, and then modify the JS and HTML files.
